## Django et Firebase

### Installation 

- Il faut cloner le projet puis faire l'install

    - sudo pip3 install Pyrebase
    - sudo pip3 install django

    - python3 manage.py migrate
    - python3 manage.py runserver

- Il faut se créer un compte sur la plateforme firebase : https://console.firebase.google.com/u/0/

- Ensuite vous devez créer un utilisateur qui doit pouvoir se connecter par email/password en allant dans :
    - Authentification > Sign-in method pour activer l'authentification adresse email/mot de passe
    - Authentification > Users et ajouter un utilisateur

- Ensuite il faut créer une realtime database

Pour finir il faut renomer le fichier "example.views.py" en "views.py" et remplacer les informations de configuration de la base de donnée par les votres.
( 'apiKey': "",
    'authDomain': "",
    'databaseURL': "",
    'projectId': "",
    'storageBucket': "",
    'messagingSenderId': "",
    'appId': "")
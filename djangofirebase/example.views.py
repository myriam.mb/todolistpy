from django.shortcuts import render
import pyrebase

config = {
    
    'apiKey': "",
    'authDomain': "",
    'databaseURL': "",
    'projectId': "",
    'storageBucket': "",
    'messagingSenderId': "",
    'appId': ""
  }

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()
db = firebase.database()

def singIn(request):

    return render(request, "signIn.html")

def postsign(request):
    email = request.POST.get("email")
    passw = request.POST.get("pass")
    
    try:
        user = auth.sign_in_with_email_and_password(email,passw)
        request.session["sessionU"] = user
    except:
        message = 'invalid credentials'
        return render(request,'signIn.html', {"msg": message})
    print(user)

    return render(request, 'welcome.html',{'e':email})

def postmess(request):

    userR = request.session["sessionU"]
    print(userR)
    
    data = {
        "mess": request.POST.get("mess")
    }

    results = db.child("messa").push(data, userR['idToken'])

    return render(request, 'mess.html', {"mess":mess})

def home(request) :

    return render(request, 'welcome.html')